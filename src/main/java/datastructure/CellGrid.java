package datastructure;

import java.util.Arrays;

import cellular.CellState;

public class CellGrid implements IGrid {
    int rows;
    int columns;
    CellState initialState;
    CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        this.initialState = initialState;
        grid = new CellState[rows][columns];

        for (int i = 0; i < rows; i += 1) {
          
                Arrays.fill( grid[i], initialState);
        
        }
		
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if ((row >= 0 && row < numRows()) && (column >= 0 && column < numColumns())) {
            grid[row][column] = element;
        }
        else {
            throw new IndexOutOfBoundsException();
        }
           
    }

    @Override
    public CellState get(int row, int column) {
        if ((row >= 0 && row < numRows()) && (column >= 0 && column < numColumns())) {
            return grid[row][column];
        }
        else {
            throw new IndexOutOfBoundsException();
        }
        
    }

    @Override
    public IGrid copy() {
        IGrid gridCopy = new CellGrid(rows, columns, initialState);

        for (int i = 0; i < rows; i += 1) {
            for (int j = 0; j < columns; j += 1) {
                gridCopy.set(i, j, grid[i][j]);
            }
    }
        return (gridCopy);
    }

    
}


