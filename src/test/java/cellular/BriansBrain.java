package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {


    /*
    * Variabler
    *
    */
    int rows;
	int columns;
	IGrid currentGeneration;

    // Constructor
    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
        this.rows = rows;
        this.columns = columns;
	
	}

    

    @Override
    public void initializeCells() {
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}   
    }

    

    @Override
    public int numberOfRows() {
        return rows;
    }

    @Override
    public int numberOfColumns() {
        return columns;
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }

    @Override
    public CellState getCellState(int row, int column) {
        CellState stateOfCell = currentGeneration.get(row, column);
		return stateOfCell;
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
		for (int i = 0; i < numberOfRows(); i += 1) {
			for (int j = 0; j < numberOfColumns(); j += 1) {
				nextGeneration.set(i, j, getNextCell(i, j));
			}
		}
		currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        CellState stateOfCell = getCellState(row, col);

		if (stateOfCell == CellState.ALIVE) {
			stateOfCell = CellState.DYING;
		}
        else if (stateOfCell == CellState.DYING) {
			stateOfCell = CellState.DEAD;
		}
        else if ((stateOfCell == CellState.DEAD) && (countNeighbors(row, col, CellState.ALIVE) == 2)) {
			stateOfCell = CellState.ALIVE;
		}
		
		return stateOfCell;
    }

    private int countNeighbors(int row, int col, CellState state) {
        int numberOfNeighbors = 0;
        for (int i = row-1; i <= row+1; i += 1) {
            for(int j = col-1; j <= col+1; j += 1) {
                if (i < 0 || j < 0) {
                    continue;
                }
                if (i >= currentGeneration.numRows() || j >= currentGeneration.numColumns() ) {
                    continue;
                }
                if (i == row && j == col) {
                    continue;
                }
                else if (currentGeneration.get(i, j).equals(state)) {
                    numberOfNeighbors += 1;
                }
            }
        }
		return numberOfNeighbors;
    }



    

}
